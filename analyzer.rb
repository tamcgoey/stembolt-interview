require 'csv'
require 'set'

# Analyzer
# a little app that analyses freedom of information (foi) requests

class Ministry
  attr_accessor :name
  attr_accessor :total_process_time
  attr_accessor :application_count
  attr_reader :avg_process_time

  def initialize(name)
    @name = name
    @total_process_time = 0
    @application_count = 0
  end

  def calc_avg_process_time
    @avg_process_time = @total_process_time / @application_count
  end


end

class Request
  # Currently only supporting the min required attributes for querying
  attr_accessor :id
  attr_accessor :ministry
  attr_accessor :start_date

  def initialize(id)
    @id = id
  end
end

class Analyzer
  attr_accessor :applicants
  attr_accessor :requests
  attr_accessor :ministries
  attr_accessor :total_process_time
  attr_accessor :requests_ended_by_month
  attr_accessor :total_request_count

  def initialize()
    @applicants = Set.new()
    @ministries = Hash.new()
    @requests = Hash.new()

    @requests_ended_by_month = Hash.new(0)
    @total_process_time = 0
    @total_request_count = 0
  end

  def stored_requests_count
    @requests.size
  end

  # Returns requests by month (converts long form to short form)
  # Defaults to December
  def requests_ended_month(month)
    if month == 'January'
      short_form = 'Jan'
    elsif month == 'February'
      short_form = 'Feb'
    elsif month == 'March'
      short_form = 'Mar'
    elsif month == 'April'
      short_form = 'Apr'
    elsif month == 'May'
      short_form = 'May'
    elsif month == 'June'
      short_form = 'Jun'
    elsif month == 'July'
      short_form = 'Jul'
    elsif month == 'August'
      short_form = 'Sug'
    elsif month == 'September'
      short_form = 'Sep'
    elsif month == 'October'
      short_form = 'Oct'
    elsif month == 'November'
      short_form = 'Nov'
    else
      short_form = 'Dec'
    end

    @requests_ended_by_month[short_form]
  end

  def average_request_time
    @total_process_time / @total_request_count
  end

  def ministry_with_highest_applications
    @ministries.values.sort_by { |m| m.application_count}.last
  end

  def ministry_with_longest_response
    @ministries.values.sort_by { |m| m.total_process_time}.last
  end
end

# Helpers

def pretty_printer(analyzer)
  puts "There are #{analyzer.total_request_count} records"
  puts "The start_date of request 'JAG-2013-00687' was "
    + " #{analyzer.requests['JAG-2013-00687'].start_date}"
  puts "Applications were received from: #{analyzer.applicants.sort.join(", ")}"

  ministry_highest_app = analyzer.ministry_with_highest_applications
  puts "The most applications were to the ministry of #{ministry_highest_app.name} "
    + "with: #{ministry_highest_app.application_count} applications"

  puts "#{analyzer.requests_ended_month('April')} requests ended in April"

  puts "Requests were processed in #{analyzer.average_request_time} on average"

  ministry_longest_response = analyzer.ministry_with_longest_response
  puts "The ministry of #{ministry_longest_response.name} took the longest to "
    + "respond, taking on average #{ministry_longest_response.avg_process_time} days"
end

def file_printer(file, analyzer)
  file.write "There are #{analyzer.total_request_count} records\n"
  file.write "The start_date of request 'JAG-2013-00687' was "
              + "#{analyzer.requests['JAG-2013-00687'].start_date}\n"
  file.write "Applications were received from: #{analyzer.applicants.sort.join(", ")}\n"

  ministry_highest_app = analyzer.ministry_with_highest_applications
  file.write "The most applications were to the ministry of "
   + "#{ministry_highest_app.name} with: #{ministry_highest_app.application_count} applications\n"

  file.write "#{analyzer.requests_ended_month('April')} requests ended in April\n"

  file.write "Requests were processed in #{analyzer.average_request_time} on average\n"

  ministry_longest_response = analyzer.ministry_with_longest_response
  file.write "The ministry of #{ministry_longest_response.name} took the longest "
    + " to respond, taking on average #{ministry_longest_response.avg_process_time} days\n"
end


# Main APP Runner

csv_path = ARGV[0] || "bccrtsfoi.csv" # support passing a data file

puts "Freedom of Information Analyzer App - Data File: #{csv_path}"

analyzer = Analyzer.new

CSV.foreach(csv_path, headers: {first_row: true}) do |req|

  # Ministry Record Keeping
  ministry = analyzer.ministries[req["Ministry"]]

  if ministry.nil?
    ministry = Ministry.new(req["Ministry"])
    analyzer.ministries[ministry.name] = ministry
  end

  ministry.application_count += 1
  ministry.total_process_time += req["Processing Days Days"].to_i
  ministry.calc_avg_process_time

  # Application Record Keeping
  request = analyzer.requests[req["Request #"]]

  if request.nil?
    request = Request.new(req["Request #"])
    analyzer.requests[request.id] = request
  end

  request.ministry = req["Ministry"]
  request.start_date = req["Start Date"]


  # Analyzer Record Keeping (Big Overall Stats)
  analyzer.total_process_time += req["Processing Days Days"].to_i

  end_month = req["End Date"][/[a-zA-Z]+/]
  analyzer.requests_ended_by_month[end_month] += 1

  analyzer.applicants.add(req["Applicant"])
  analyzer.total_request_count += 1

end

output_file_path = ARGV[1] # Set output file path

if output_file_path.nil?
  pretty_printer(analyzer)
else
  f = File.new(output_file_path, "w")
  file_printer(f, analyzer)
  f.close

  puts "Analyzer Finished Writing to - #{output_file_path}"
end
